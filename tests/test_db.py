import unittest

from mysql.connector import errors

from db_interface import get_sql_connector


class TestDB(unittest.TestCase):

    def test_connectivity(self):
        con = None
        try:
            con = get_sql_connector()
        except:
            pass
        finally:
            assert con is not None

    def test_unique_constraint(self):
        with self.assertRaises(errors.IntegrityError):
            con = get_sql_connector()
            cursor = con.cursor()
            cursor.execute("INSERT INTO categories (name, foreign_id, parent_id) VALUES ('test', '01', '00')")
            con.commit()
            con.close()
