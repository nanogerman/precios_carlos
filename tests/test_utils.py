import unittest
from utils import extract_digits_from_string


class TestUtils(unittest.TestCase):

    def test_string_not_contains_int_1(self):
        striped = extract_digits_from_string("I'm2a3test4string")
        assert striped == "234"

    def test_string_not_contains_int_2(self):
        striped = extract_digits_from_string("I'm a test string")
        assert striped == ""

    def test_string_not_contains_int_3(self):
        striped = extract_digits_from_string("234435")
        assert striped == "234435"
