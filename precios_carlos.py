import argparse
import collections
import logging

import requests

from db_interface import *
from scraper import Scraper
from settings import ENVIRONMENT
from utils import *

API_KEY = 'API_KEY'
API_URL = 'API_URL'
EXCHANGE_URL = 'https://openexchangerates.org/'
EXCHANGE_URL_ID = 'https://openexchangerates.org/api/latest.json?app_id='
API_KEY_EXCHANGE = '2f83829203f84eb7a37d34e1c05ec7e1'

TYPE_PRODUCT = 'productos'
TYPE_STORE = 'sucursales'

ARG_CATEGORY = 0
ARG_STORE = 1

BAD_REQUEST = 400

DEPTH_CHILD_CATEGORY = 4

LOG_FILENAME = 'precios_carlos.log'


def get_exchange():
    """
    Get the current date exchange for USD-ARG
    :return: FLOAT
    """
    exchange = requests.get(EXCHANGE_URL_ID + API_KEY_EXCHANGE)
    if exchange.status_code != requests.codes.ok:
        raise Exception('Could not retrieve URL information for: {}.. Error code: {}'.format(EXCHANGE_URL, exchange.status_code))

    parsed_data = parse_json_response(exchange.content)
    value_exchange = parsed_data['rates']['ARS']
    logging.debug("Get exchange rate OK")
    logging.info("Initialized connection to DB for exchange rate")
    insert_currency_exchange(value_exchange)
    logging.debug("Insert exchange rate to DB status OK")
    print("The exchange rate for ARS to USD: ", value_exchange)


def get_categories(category, scraper, page):
    logging.info("Initialized request categories")
    categories = scraper.get_categories(page)
    logging.debug("Get categories status OK")
    if category == 'all':
        print_all_items(categories)
        logging.info("Initialized connection to DB for categories")
        categories = collections.OrderedDict(sorted(categories.items()))
        insert_categories(categories)
        logging.debug("Insert categories to DB status OK")
    elif category in categories.keys():
        print('Category ID {}:'.format(category))
        print_all_items(categories[category])
    else:
        raise ValueError('Category ID {} not found.'.format(category))


def get_stores(store, scraper, api_url, api_key):
    logging.info("Initialized request stores")
    stores = scraper.get_products_or_stores(api_url, api_key, TYPE_STORE)
    logging.debug("Get stores status OK")
    if store == 'all':
        print_all_items(stores)
        logging.info("Initialized connection to DB for stores")
        insert_stores(stores)
        logging.debug("Insert stores to DB status OK")
    elif store in stores.keys():
        print("Store ID {}:".format(store))
        print_all_items(stores[store])
    else:
        raise ValueError("Store ID {} not found.".format(store))


def get_products(product, scraper, api_url, api_key):
    logging.info("Initialized request products")
    products = scraper.get_products_or_stores(api_url, api_key, TYPE_PRODUCT, product[ARG_CATEGORY],
                                              product[ARG_STORE])
    print_all_items(products)
    logging.debug("Get products status OK")
    if len(product[ARG_CATEGORY]) < DEPTH_CHILD_CATEGORY:
        logging.warning("Category ID must be level 2 to be inserted into DB")
        print("Category ID must be level 2 to be inserted into DB")
    else:
        logging.info("Initialized connection to DB for products")
        if len(product) == 0:
            insert_products(products, product[ARG_CATEGORY], product[ARG_STORE])
            logging.debug("Insert products to DB status OK")
        else:
            logging.warning("The store id {} has not products in category id {}".format(product[ARG_STORE],
                                                                                        product[ARG_CATEGORY]))


def main():
    """Scrap www.preciosclaros.gob.ar page, extract Categories, Stores and Products and print results on screen"""
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', nargs='?', const='all', help='Id of CATEGORY - Represents a category. Empty value '
                                                           'returns all categories. Example: 0102')
    parser.add_argument('-s', nargs='?', const='all', help='Id of a STORE - Represents a Store. Empty value returns '
                                                           'all stores. Example: 2-1-031')
    parser.add_argument('-e', nargs='?', const='all', help='Exchange value - Represents the exchagne rate beteween USD '
                                                           'and ARS.')
    parser.add_argument('-p', nargs=2, help='Products. Return all products for a specific Category and Store. First '
                                            'parameter represents a Category (example: 0203); Second parameter '
                                            'represents a Store (example: 2-1-032)')
    parser.add_argument('-db', nargs='?', const='create', help='Creates DB Schema')
    parser.add_argument('-a', nargs='?', const='create', help='Populate all the DB with textfile input')

    args = parser.parse_args()

    logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG, format='%(asctime)s - %(levelname)s - :%(message)s')
    logging.info("*" * 24 + "S T A R T" + "*" * 24 + "\n")

    if args.db is not None:
        try:
            if ENVIRONMENT == 'DEV':
                logging.info("Initialized DB creation")
                initialize_db()
                print("DB created successfully")
                logging.debug("DB created successfully")
            else:
                print("Cannot create a DB in Prod Environment")
                logging.warning("Cannot create a DB in Prod Environment")
        except Exception as ex:
            print(ex.args)
            exit(1)

    if args.c is None and args.s is None and args.p is None and args.e is None and args.a is None:
        parser.print_help()
        logging.warning("Need to add at least one argument")
        exit(1)

    try:
        scraper = Scraper()

        logging.info("Initialized request api url and api key")
        page_content = scraper.get_main_page_content()
        api_url = scraper.get_api_value(page_content, API_URL)
        api_key = scraper.get_api_value(page_content, API_KEY)
        logging.debug("Api url " + api_url + " and api key " + api_key)
        if args.c:
            get_categories(args.c, scraper, page_content)

        if args.s:
            get_stores(args.s, scraper, api_url, api_key)

        if args.p:
            get_products(args.p, scraper, api_url, api_key)

        if args.e:
            get_exchange()

        if args.a is not None:
            scraper.populate_db(api_url, api_key, args.a)

        logging.info("*" * 24 + "F I N I S H" + "*" * 24 + "\n")
    except Exception as e:
        logging.critical(str(e))
        print(e.args[0])
        raise


if __name__ == '__main__':
    main()
