import logging
import re

import requests

from db_interface import *
from utils import *

SOURCE_URL = 'https://www.preciosclaros.gob.ar/#!/buscar-productos'
STORES_URL = '/sucursales'
PRODUCTS_URL = '/productos'
HEADER_API_KEY = 'x-api-key'
HEADER_USER_AGENT = 'User-Agent'
HEADER_USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) " \
                          "Chrome/22.0.1207.1 Safari/537.1"
API_KEY = 'API_KEY'
API_URL = 'API_URL'
EXCHANGE_URL = 'https://openexchangerates.org/'
EXCHANGE_URL_ID = 'https://openexchangerates.org/api/latest.json?app_id='
API_KEY_EXCHANGE = '2f83829203f84eb7a37d34e1c05ec7e1'

TYPE_PRODUCT = 'productos'
TYPE_STORE = 'sucursales'

ARG_CATEGORY = 0
ARG_STORE = 1

BAD_REQUEST = 400

DEPTH_CHILD_CATEGORY = 4
LOG_FILENAME = 'precios_carlos.log'

logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG, format='%(asctime)s - %(levelname)s - :%(message)s')


class Scraper:
    def get_products_or_stores(self, api_url, api_key, get_type, category_id=None, store_id=None):
        """
        If 'get_type' received is TYPE_PRODUCT:  Get all products of the given category and store
        If 'get_type' received is TYPE_STORE:  Get all stores
        :param api_url: STRING
        :param api_key: STRING
        :param get_type: STRING
        :param category_id: STRING
        :param store_id: STRING
        :return: DICT
        """
        product_dict = {}

        url = api_url + (PRODUCTS_URL if get_type == TYPE_PRODUCT else STORES_URL)
        headers = {HEADER_API_KEY: api_key, HEADER_USER_AGENT: HEADER_USER_AGENT_VALUE}

        if get_type == TYPE_PRODUCT:
            payload = {'id_sucursal': store_id, 'id_categoria': category_id}
            data_page = requests.get(url=url, headers=headers, params=payload)
        else:
            data_page = requests.get(url=url, headers=headers)

        if data_page.status_code != requests.codes.ok:
            raise Exception('Could not retrieve URL information for: {}.. Error code: {}'
                            .format(url, data_page.status_code))
        if "error" in data_page.text:
            raise Exception('Could not retrieve URL information due to time out')

        parsed_data = parse_json_response(data_page.content)
        if parsed_data['status'] == BAD_REQUEST:
            raise ValueError("Store ID {} or Category ID {} is not find.".format(store_id, category_id))

        max_limit_allowed = parsed_data['maxLimitPermitido']
        total = parsed_data['total']
        offset = 0
        limit = offset + max_limit_allowed - 1

        while offset < total:
            if get_type == TYPE_PRODUCT:
                payload = {'id_sucursal': store_id, 'id_categoria': category_id, 'offset': offset, 'limit': limit}
            else:
                payload = {'offset': offset, 'limit': limit}

            data_page = requests.get(url=url, headers=headers, params=payload)
            parsed_data = parse_json_response(data_page.content)
            product_dict.update({x['id']: {key: value for key, value in x.items() if key != 'id'}
                                 for x in parsed_data[TYPE_PRODUCT if get_type == TYPE_PRODUCT else TYPE_STORE]})
            offset += max_limit_allowed
            limit += max_limit_allowed

        return product_dict

    def format_category_id(self, categories):
        """
        Generate a dictionary from list.. Format category ids in required format:
            - Category: dd
            - Subcategory_1: dddd
            - Subcategory_2: dddddd
        :param categories: LIST
        :return: DICT
        """
        categories_dict = {}
        subcategory_id = 0
        for category in categories:
            if category['parent_id'] is None:
                subcategory_id = 0
                category['id'] = category['id'].zfill(2)
            else:
                category['parent_id'] = category['parent_id'].zfill(2)
                category['id'] = category['parent_id'] + str(subcategory_id).zfill(2)
            subcategory_id += 1
            categories_dict[category['id']] = {'parent_id': category['parent_id'], 'name': category['name']}

        return categories_dict

    def get_categories(self, page_content):
        """
        Extract from html page all categories and subcategories.
        :param page_content: STRING
        :return: DICT
        """
        categories = [{'id': extract_digits_from_string(item.attrs['href']),
                       'parent_id': extract_digits_from_string(item.parent.attrs['id'])
                       if item.attrs['href'].startswith('#sub') else None, 'name': item.text.strip()}
                      for item in page_content.find_all("a", {"class": lambda L: L and L.endswith('categoria')})]
        categories_dict = self.format_category_id(categories)
        return categories_dict

    def get_api_value(self, page_content, api_value):
        """
        Scrap the page to get the API_KEY needed for future requests
        :param page_content: STRING
        :param api_value: STRING
        :return: STRING
        """
        return [matched_api_value.groups(0)[0] for matched_api_value in
                (re.search(r'var {} = \"(.*)\"'.format(api_value), script.text)
                 for script in page_content.find_all('script')) if matched_api_value is not None][0]

    def get_main_page_content(self):
        """
        Get main page
        :return: BeautifulSoup
        """
        main_page = requests.get(SOURCE_URL)

        if main_page.status_code != requests.codes.ok:
            raise Exception('Could not retrieve URL information for: {}.. Error code: {}'
                            .format(SOURCE_URL, main_page.status_code))

        parsed_html = BeautifulSoup(main_page.content, 'html.parser')

        return parsed_html

    def populate_db(self, api_url, api_key, file=None):
        """"
        Get all the stores and categories avaliable to insert all the products into DB
        :return:
        """
        all_categories = select_categories_stores()
        if file:
            with open(file) as f:
                content = f.readlines()
            # you may also want to remove whitespace characters like `\n` at the end of each line
            stores_selected = [x.strip() for x in content]

        all_categories = [i[0] for i in all_categories]
        for category in all_categories:
            for store in stores_selected:
                try:
                    products = self.get_products_or_stores(api_url, api_key, TYPE_PRODUCT, category, store)
                    if len(products) > 0:
                        insert_products(products, category, store)
                except Exception as ex:
                    logging.warning(str(ex))
