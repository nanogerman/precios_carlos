import datetime

import mysql.connector

from settings import *


def drop_db():
    """
    Esta es la prueba del branch test
    :return:
    """
    con_string = get_connection_string()
    con = mysql.connector.connect(user=con_string['user'], password=con_string['pass'], host=con_string['host'],
                                  port=con_string['port'])
    cursor = con.cursor()

    # Below line hides warnings
    cursor.execute("SET sql_notes = 0; ")

    cursor.execute("DROP DATABASE IF EXISTS " + con_string['db'])
    con.commit()
    con.close()

def feature1():
    print("Feature 1")

def insert_stores(stores_list):
    """"
    Agregue comentatios 2
    """
    query_insert_stores = "INSERT IGNORE INTO stores (foreign_id, brand, name, type, address, city, province, " \
                          "latitude, longitude) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    con = get_sql_connector()
    creation_cursor = con.cursor()
    for index, val in enumerate(stores_list):
        value = (val, stores_list[val]['banderaDescripcion'], stores_list[val]['sucursalNombre'],
                 stores_list[val]['sucursalTipo'],
                 stores_list[val]['direccion'], stores_list[val]['localidad'], stores_list[val]['provincia'],
                 stores_list[val]['lat'],
                 stores_list[val]['lng'])
        creation_cursor.execute(query_insert_stores, value)
    con.commit()
    con.close()

def feature2():
    """
    Comment
    :return:
    """
    print("Feature 2")

def create_db():
    con_string = get_connection_string()
    con = mysql.connector.connect(user=con_string['user'], password=con_string['pass'], host=con_string['host'],
                                  port=con_string['port'])
    cursor = con.cursor()

    # Below line hides warnings
    cursor.execute("SET sql_notes = 0; ")

    cursor.execute("CREATE DATABASE IF NOT EXISTS " + con_string['db'])
    con.commit()
    con.close()


def get_sql_connector():
    con_string = get_connection_string()
    return mysql.connector.connect(user=con_string['user'], password=con_string['pass'], host=con_string['host'],
                                   port=con_string['port'], database=con_string['db'])


def create_tables():
    query_create_stores = "CREATE TABLE stores (" \
                          "id int(11) NOT NULL AUTO_INCREMENT COMMENT " \
                          "'Identification of the Store. It iss incremental.', " \
                          "foreign_id varchar(45) NOT NULL COMMENT " \
                          "'Identification of the store at www.preciosclarios.gob.ar, it is called id.', " \
                          "brand varchar(45) DEFAULT NULL COMMENT " \
                          "'Brands name of the store. At www.precioscarlos.gob.ar the variable is called bandera_descr.', " \
                          "name varchar(128) DEFAULT NULL COMMENT " \
                          "'The name of the Store. At www.preciosclaros.gob.ar is called sucursalNombre.', " \
                          "type varchar(45) DEFAULT NULL COMMENT " \
                          "'Types: upermerket/hipermarket/small market. At www.precioscarlos.gob.ar is called sucursal_tipo.', " \
                          "address varchar(256) DEFAULT NULL COMMENT " \
                          "'Address of the location of the store. At www.preciosclaros.gob.ar is called direccion.', " \
                          "city varchar(128) DEFAULT NULL COMMENT " \
                          "'City where the store is located. At www.precioscarlos.gob.ar is called localidad.', " \
                          "province varchar(128) DEFAULT NULL COMMENT " \
                          "'Province where the store is located. At www.precioscarlos.gob.ar is called provincia.', " \
                          "latitude varchar(45) DEFAULT NULL COMMENT " \
                          "'The latitud of the store location.', " \
                          "longitude varchar(45) DEFAULT NULL COMMENT " \
                          "'The longitud of the store location.', " \
                          "PRIMARY KEY (id), " \
                          "UNIQUE KEY u_store_id (id), " \
                          "UNIQUE KEY u_store_foreign_id (foreign_id)) " \
                          "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci " \
                          "COMMENT='List of all Stores with their details';"

    query_create_categories = "CREATE TABLE categories (" \
                              "id int(11) NOT NULL AUTO_INCREMENT, " \
                              "name varchar(128) DEFAULT NULL COMMENT 'Name of the category.', " \
                              "foreign_id varchar(45) NOT NULL COMMENT " \
                              "'The identification of categories at www.preciosclaros.gob.ar', " \
                              "parent_id int(11) DEFAULT NULL COMMENT " \
                              "'This is the variable to create the hierarchy with other categories.'," \
                              "PRIMARY KEY (id)," \
                              "UNIQUE KEY u_category_id (id), " \
                              "UNIQUE KEY u_category_foreign_id (foreign_id)) " \
                              "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci " \
                              "COMMENT='List of all Categories with their details and hierarchy.';"

    query_create_products = "CREATE TABLE products (" \
                            "id int(11) NOT NULL AUTO_INCREMENT, " \
                            "foreign_id varchar(45) NOT NULL COMMENT " \
                            "'Stock Keeping Unit (SKU) is the code bar of the product. At www.precioscarlos.gob.ar is called id.', " \
                            "name varchar(128) DEFAULT NULL COMMENT " \
                            "'Name of the products. At www.preciosclaros.gob.ar is called nombre.', " \
                            "dimension varchar(45) DEFAULT NULL COMMENT " \
                            "'Dimension and type of dimension: ml/kg/units. At www.precioscarlos.gob.ar is called presentacion.', " \
                            "category_id int(11) NOT NULL COMMENT " \
                            "'Is the identification of the category that the product belongs.', " \
                            "brand varchar(45) DEFAULT NULL COMMENT " \
                            "'Brand of the product. At www.preciosclaros.gob.ar is called marce.', " \
                            "PRIMARY KEY (id), " \
                            "UNIQUE KEY u_product_id (id), " \
                            "UNIQUE KEY u_product_foreign_id (foreign_id), " \
                            "KEY idx_fk_categories (category_id), " \
                            "CONSTRAINT fk_categories FOREIGN KEY (category_id) REFERENCES categories (id)) " \
                            "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci " \
                            "COMMENT='List of all Products with their details';"

    query_create_h_prices = "CREATE TABLE historical_prices (" \
                            "id int(11) NOT NULL AUTO_INCREMENT, " \
                            "date datetime NOT NULL COMMENT " \
                            "'This is the date when a price is change for a specific product in a store.', " \
                            "product_id int(11) NOT NULL COMMENT " \
                            "'This is the reference for a specific product.', " \
                            "store_id int(11) NOT NULL COMMENT " \
                            "'This is the reference of the store where the product belong in the price change.', " \
                            "price decimal(20,2) NOT NULL COMMENT " \
                            "'Price of 1 product for 1 store in a specifif date.', " \
                            "PRIMARY KEY (id), " \
                            "UNIQUE KEY u_historical_price_id (id), " \
                            "UNIQUE KEY u_historical_price_combination (date, product_id, store_id), " \
                            "KEY idx_fk_products (product_id), " \
                            "KEY idx_fk_store (Store_Id), " \
                            "CONSTRAINT fk_products FOREIGN KEY (product_id) REFERENCES products (id), " \
                            "CONSTRAINT fk_stores FOREIGN KEY (store_id) REFERENCES stores (id)) " \
                            "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci " \
                            "COMMENT='This is where all the prices will be stored.';"

    query_create_currency_exchange = "CREATE TABLE `precios_carlos`.`currency_exchange` (" \
                                     "id INT NOT NULL AUTO_INCREMENT," \
                                     "date DATETIME NULL COMMENT 'Date of the exchange value requested', " \
                                     "currency_code VARCHAR(45) NULL COMMENT 'Currency convertion code', " \
                                     "exchange_value FLOAT NULL COMMENT 'Rate value for a specific day.', " \
                                     "PRIMARY KEY (id), " \
                                     "UNIQUE KEY u_currency_id (id), " \
                                     "UNIQUE KEY u_currency_date (date)) " \
                                     "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci " \
                                     "COMMENT='Stores foreign currency exchange with ARS (Argentinian Pesos)';"

    con = get_sql_connector()
    creation_cursor = con.cursor()
    creation_cursor.execute(query_create_stores)
    creation_cursor.execute(query_create_categories)
    creation_cursor.execute(query_create_products)
    creation_cursor.execute(query_create_h_prices)
    creation_cursor.execute(query_create_currency_exchange)
    con.commit()
    con.close()


def initialize_db():
    drop_db()
    create_db()
    create_tables()


def insert_currency_exchange(rate, currency_code="USD"):
    sql_insert_currency_rate = "INSERT INTO currency_exchange (date, currency_code, exchange_value ) VALUES (%s, %s, %s) ;"
    sql_update_currency_rate = "UPDATE currency_exchange SET exchange_value = %s WHERE date = %s AND currency_code = %s ;"
    today = datetime.datetime.today().date()

    con = get_sql_connector()

    cur = con.cursor()
    cur.execute("SELECT * FROM currency_exchange WHERE currency_code = %s AND date = %s", [currency_code, today])
    currency_value = cur.fetchone()
    if currency_value:
        cur.execute(sql_update_currency_rate, [rate, today, currency_code])
    else:
        cur.execute(sql_insert_currency_rate, [today, currency_code, rate])
    con.commit()
    con.close()


def insert_categories(category_list):
    sql = "INSERT IGNORE INTO categories (name, foreign_id, parent_id) VALUES (%s, %s, %s)"

    con = get_sql_connector()
    cur = con.cursor()
    cur_2 = con.cursor()

    for cat_id, cat_attrs in category_list.items():
        if cat_attrs['parent_id'] is None:
            val = (cat_attrs["name"], cat_id, None)
            cur.execute(sql, val)
            cur_2.execute("SELECT id FROM categories WHERE foreign_id = %s", [cat_id])
            parent_id = cur_2.fetchone()[0]
        else:
            val = (cat_attrs["name"], cat_id, parent_id)
            cur.execute(sql, val)
        con.commit()
    con.close()


def insert_products(products, foreign_cat_id, foreign_store_id):
    sql_product = "INSERT IGNORE INTO products (foreign_id, name, dimension, category_id, brand) " \
                  "VALUES (%s, %s, %s, %s, %s)"
    sql_h_prices = "INSERT INTO historical_prices (date, product_id, store_id, price ) " \
                   "VALUES (%s, %s, %s, %s) ON DUPLICATE KEY UPDATE price = VALUES(price);"

    con = get_sql_connector()

    cur_1 = con.cursor()
    cur_1.execute("SELECT ID FROM categories WHERE foreign_id = %s", [foreign_cat_id])
    cat_id = cur_1.fetchone()[0]

    cur_2 = con.cursor()
    cur_2.execute("SELECT ID FROM stores WHERE foreign_id = %s", [foreign_store_id])
    store_id = cur_2.fetchone()[0]
    today = datetime.datetime.today().date()

    cur_3 = con.cursor()
    cur_4 = con.cursor()
    for foreign_prod_id, prod_attrs in products.items():
        val_prod = (foreign_prod_id, prod_attrs['nombre'], prod_attrs['presentacion'], cat_id, prod_attrs['marca'])
        cur_3.execute(sql_product, val_prod)
        con.commit()
        if prod_attrs['precio']:
            prod_id = select_product_id(foreign_prod_id)
            val_hist_prices = (today, str(prod_id), store_id, round(prod_attrs['precio'], 2))
            cur_4.execute(sql_h_prices, val_hist_prices)

    con.commit()
    con.close()


def select_categories_stores():
    con = get_sql_connector()
    cur = con.cursor()
    cur.execute("SELECT foreign_id FROM categories WHERE parent_id > 0")
    categories_id = cur.fetchall()
    con.close()

    return categories_id


def select_product_id(foreign_id):
    sql = "SELECT id FROM products WHERE foreign_id = %s"

    con = get_sql_connector()
    cur = con.cursor()
    cur.execute(sql, [foreign_id])
    cursor_output = cur.fetchone()
    product_id = cursor_output[0]
    con.close()

    return product_id
