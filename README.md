# Data Mining project - PRECIOS CARLOS!

### A tool to get insights about the product market prices in Argentina.

## Tools:

* Python
* Requests
* BeautifulSoup4
* Mysql

## Instructions:
The first step to execute must be ./precios_carlos.py -db in order to create the 
database in Mysql and the installation of resources.txt.
**Configure your mysql settings in the file settings/dev.py**

### Instalation:
Before running the script it is required to install resources.txt

    pip install -r resources.txt
    python ./precios_carlos.py -db

### Bitbucket:

All the project creation process is stored at BitBucket.

    git clone https://DiegoArabCohen@bitbucket.org/nanogerman/precios_carlos.git

### Execution:

Usage:

    python ./precios_carlos.py [-h] [-c [C]] [-s [S]] [-p P P] [-db] [-e] [-a]

Optional arguments:

    -h, --help  show this help message and exit

    -c [C]      Id of CATEGORY - Represents a category. Empty value
                returns all categories. Example: 0102

    -s [S]      Id of a STORE - Represents a Store. Empty value returns
                all stores. Example: 2-1-031
                
    -e [E]      Exchange value - Represents the exchagne rate beteween 
                USD and ARS.

    -p P P      Products. Return all products for a specific Category
                and Store. First parameter represents a Category
                (example: 0203); Second parameter represents a Store
                (example: 2-1-032)
    
    -db         Database. Create the database structure with the tables
                for the categories, stores, products and prices. If the 
                database exists it will erase and create a new one.

    -a          Populate all the DB with textfile input.
                example: ./feed_data/store_group_1.txt


Examples:

    python ./precios_carlos.py -db
    python ./precios_carlos.py -e
    python ./precios_carlos.py -c
    python ./precios_carlos.py -c 0102
    python ./precios_carlos.py -s
    python ./precios_carlos.py -s 6-2-2
    python ./precios_carlos.py -c -s
    python ./precios_carlos.py -c 01 -s
    python ./precios_carlos.py -c -s 6-1-22
    python ./precios_carlos.py -c 01 -s 6-1-11
    python ./precios_carlos.py -p 0201 6-1-19
    python ./precios_carlos.py -c -s -p 0302 6-1-17
    python ./precios_carlos.py -a store_group_2.txt

### Population on AWS

Data will be feed following the next configurations:

    -c      Once a day. Time: 2am
    -s      Once a day. Time: 2am
    -e      Once a day. Time: 2am
    -a      Once a day. Time: 2.30am

### Results:

It will print:

    - If '-c' command is present; All the Categories (included
    subcategories) or a specific category if it comes as parameter.

    - If '-s' command is present; All the Stores or a specific store
    if it comes as parameter.
    
    - If '-e' command is present; It will update the exchange rate
    beteween USD and ARS.

    - If '-p' command is present; A list of all the Products of a
    specific Category, which are present in a specific Store.
    (Category and Store values are mandatory parameters).

    - If '-a' command is present; A list of all the Products of all the
    categories, which are present in a specifics stores. Those stores
    are loaded from a textfile.

    - If '-db' command is present; Create database called precios_carlos.
    It will print a message if the database was created correctly.


### Logging:
The script precios_carlos.py will create a file called precios_carlos.log that
will record all the steps staring with a comment " START " and ending with
" FINISH " if the script will not have a critical problem. The format of the
logging is structured with datetime, level and message.
There are 4 levels of messages:
* Info: Message that a new process began
* Debug: Message of what is doing the process
* Warnings: Message of invalid store id or category id requested
* Critical: Message when the process crashed


    2018-11-28 17:02:57,444 - INFO - :************************ S T A R T ************************
    2018-11-28 17:02:57,446 - INFO - :Initialized request api url and api key
    2018-11-28 17:02:57,453 - DEBUG - :Starting new HTTPS connection (1): www.preciosclaros.gob.ar:443
    2018-11-28 17:02:59,519 - DEBUG - :https://www.preciosclaros.gob.ar:443 "GET / HTTP/1.1" 200 279214
    2018-11-28 17:03:00,738 - DEBUG - :Api url https://d735s5r2zljbo.cloudfront.net/prod and api key zIgFou7Gta7g87VFGL9dZ4BEEs19gNYS1SOQZt96
    2018-11-28 17:03:00,739 - INFO - :Initialized request stores
    2018-11-28 17:03:00,742 - DEBUG - :Starting new HTTPS connection (1): d735s5r2zljbo.cloudfront.net:443
    2018-11-28 17:03:01,440 - DEBUG - :https://d735s5r2zljbo.cloudfront.net:443 "GET /prod/sucursales HTTP/1.1" 200 3731
    2018-11-28 17:03:01,461 - DEBUG - :Starting new HTTPS connection (1): d735s5r2zljbo.cloudfront.net:443
    ...
    2018-11-28 17:03:55,846 - DEBUG - :Get stores status OK
    2018-11-28 17:03:56,181 - INFO - :Initialized connection to DB for stores
    2018-11-28 17:03:59,118 - DEBUG - :Insert stores to DB status OK
    2018-11-28 17:03:59,120 - INFO - :************************ F I N I S H ************************

## Owners:
* Diego Arab Cohen
* Mariano German

