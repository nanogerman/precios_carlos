import json

from bs4 import BeautifulSoup


def print_all_items(items):
    """
    Print a list of the first 10 items of stores/products
    :param items: DICT
    """
    for k, v in items.items():
        print(k, v)


def extract_digits_from_string(word):
    """
    Given an string, strip all non-numerical characters.
    :param word: STRING
    :return: STRING
    """
    return ''.join(filter(lambda x: x.isdigit(), word))


def parse_json_response(page_content):
    """
    Receive a json string, parse it
    :param page_content: STRING
    :return: DICT
    """
    parsed_page = BeautifulSoup(page_content, 'html.parser')
    return json.loads(parsed_page.prettify())
